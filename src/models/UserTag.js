const mongoose = require("mongoose");

const userTagSchema = new mongoose.Schema({
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  tagID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Tag",
    required: true,
  },
  quantity: {
    type: Number,
    required: false,
  },
});

mongoose.model("UserTag", userTagSchema);
