const { cloudinary } = require("../utils/cloudinary");
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Album = mongoose.model("Album");
const Photo = mongoose.model("Photo");
const User = mongoose.model("User");
const Tag = mongoose.model("Tag");
const UserTag = mongoose.model("UserTag");
const upload = require("../utils/multer");
const bodyParser = require("body-parser");
var cors = require("cors");
const checkIfTagExist = require("../utils/checkIfTagExist");
var recombee = require("recombee-api-client");
router.use(bodyParser.urlencoded({ extended: true }));
router.use(express.static("public"));
router.use(express.json({ limit: "50mb" }));
router.use(express.urlencoded({ limit: "50mb", extended: true }));
router.use(cors());

// router.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

router.get("/api/images", async (req, res) => {
  //Not tested yet
  const { resources } = await cloudinary.search
    .expression("folder:dev_setups")
    .sort_by("public_id", "desc")
    .max_results(30)
    .execute();

  const publicIds = resources.map((file) => file.public_id);
  res.send(publicIds);
});

//method to upload single or several images
router.post("/api/upload", upload.array("photos", 12), async (req, res) => {
  try {
    //Get the request data
    const { albumId, private, lat, long, alt, timestamp } = req.body;

    var uploadedPhotos = [];

    //Get the photos as files
    const photos = req.files;
    /*-----------Generate the required Photo scheme data-------*/

    //Get the userID from the album path
    const userID = req.user._id;

    const coords = {
      latitude: lat,
      longitude: long,
      altitude: alt,
    };

    //Get the user
    author = await User.findOne({ _id: userID });
    if (!author) {
      //Send error msg
      res.status(400).json({
        message: "User not found",
      });
      return;
    }

    var album = await Album.findOne({ _id: albumId });
    if (!album) {
      //Send error msg
      res.status(400).send({
        message: "Album not found",
      });
      return;
    }

    //Upload all photos to cloud
    responses = [];
    for (const file of photos) {
      const newPath = await cloudinary.uploader.upload(file.path, {
        folder: albumId,
      });
      responses.push(newPath);
    }

    //Save in mongo
    for (const response of responses) {
      const url = response.url;

      const public_id = response.public_id;
      const photo = new Photo({
        url,
        public_id,
        album,
        author,
        private,
        timestamp,
        coords,
      });

      // photo.tags = [];
      await photo
        .save()
        .catch((err) => res.status(422).send({ error: err.message }));

      uploadedPhotos.push(photo._id);
      album.photos.push(photo);
      await album
        .save()
        .catch((err) => res.status(422).send({ error: err.message }));
    }

    //Return a msg to client that uploaded - unnecessary for production version
    res.status(200).json({
      message: "images uploaded successfully",
      data: uploadedPhotos,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ err: "Something went wrong" });
  }
});

router.post("/api/deletephoto", async (req, res) => {
  //Not tested yet - need to store the public id in the db first
  const image_public_id = req.body.public_id;
  const image_id = req.body.image_id;
  const albumID = req.body.albumID;
  try {
    const response = await cloudinary.uploader.destroy(image_public_id);
    if (response === "not found") {
      return res.status(400).json("Image not found");
    } else {
      const response2 = await Photo.deleteOne({ _id: image_id }).then(() => {
        Album.updateOne(
          { _id: albumID },
          { $pull: { photos: image_id } },
          function (err, ii) {
            if (err) {
              return res.status(400).json("Image not found");
            }
            return res.status(200).json("Delete Image successfuly");
          }
        );
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ err: "Something went wrong" });
  }
});

router.post("/api/deleteAlbum", async (req, res) => {
  const albumName = req.body.albumName;
  //Delete all the images in the album by prefix
  cloudinary.api.delete_resources_by_prefix(`${albumName}/`, function (err) {
    if (err && err.http_code !== 404) {
      return res.status(400).json("error while deleting images");
    }

    //Delete the folder it self
    cloudinary.api.delete_folder(`${albumName}`, function (error, result) {
      if (error && error.http_code !== 404) {
        return res.status(400).json("error when delete album");
      }
      return res.status(200).json("Delete album succesfuly");
    });
  });
});

router.post("/api/createalbum", async (req, res) => {
  try {
    //NOTE - there is no method for simply create empty folder in cloudinary,
    //the most close to is to upload file to un-existing folder and then cloudinary generate the folder.
    //The solution - post a request to upload default photo to un-existing folder and then delete the photo.
    //The folders has to be unique so lets generate the name like so - `${user identifer}${album name}`

    var { albumName, partners, description, photos } = req.body;
    if (!photos) photos = [];
    if (!partners) partners = [];

    const photosQuantity = photos.length;
    const userID = req.user._id;
    const combinedName = `${userID}~${albumName}`;

    if (!albumName || !userID) {
      return res.status(422).send({ error: "You must provide a name" });
    }

    const album = await new Album({
      name: combinedName,
      description,
      partners,
      photos,
      photosQuantity,
    });

    // method to create the album in cloudinary
    await cloudinary.api.create_folder(album._id.toString());
    await album.save();
    res.send(album);
  } catch (err) {
    console.error(err);
    res.status(500).send({ err: "Something went wrong" });
  }
});

router.get("/api/recommendation", async (req, res) => {
  var hashMapAlg = new Map();

  //1.Getting the user ID
  var userID = req.user._id;
  var userLat = req.query.latUser;
  var userLong = req.query.longUser;

  //2.Getting all the UserTag of specific user
  var UserTagList = await UserTag.find({ userID: userID });

  //3.Filter -> Taking the 5 top recommended of the user Tags
  var top5 = UserTagList.sort(
    (item1, item2) => item2.quantity - item1.quantity
  ).slice(0, 5);

  //4.Looping on the 5 top tags ->
  //                            Looping the photos array of each tag ->
  //                                                                    Calling the calcCrow(userLocation,photoLocation)

  for (var i = 0; i < top5.length; i++) {
    var userTagObject = top5[i];

    var tagOfTop5 = userTagObject.tagID;
    var photosOfSpecificTag = await Tag.find({ _id: tagOfTop5 }).select(
      "photos"
    );

    for (var j = 0; j < photosOfSpecificTag.length; j++) {
      var item = photosOfSpecificTag[j];

      // item => {"_id:....","photos:[......]"}
      var arrOfPhotosGoodDistance = [];

      //looping the photos of each top tag and calculating the distance between the pictures to the current user
      for (var t = 0; t < item.photos.length; t++) {
        var photo_id = item.photos[t];
        //long + lat of picture :
        var specificPhoto = await Photo.findOne({
          _id: mongoose.Types.ObjectId(photo_id),
        });
        if (!specificPhoto.coords) {
          var longOfPhoto = specificPhoto.coords.longitude;
          var latOfPhoto = specificPhoto.coords.latitude;

          //Distance between photo to user
          var distance = calcCrow(userLat, userLong, latOfPhoto, longOfPhoto);

          if (distance < 30) {
            // For now its 10KM -> later we need to give the user the oppertuninty to choose the distance
            arrOfPhotosGoodDistance.push(photo_id);
            hashMapAlg[tagOfTop5] = arrOfPhotosGoodDistance;
          }
        }
      }
    }
  }

  const tagObjectsList = [];
  const keysMap = Object.keys(hashMapAlg);
  for (var i = 0; i < keysMap.length; i++) {
    const tag = await Tag.findOne({ _id: keysMap[i] });
    tagObjectsList.push(tag);
  }

  res.send({
    tagsList: Object.keys(hashMapAlg),
    tagPhotos: Object.values(hashMapAlg),
    tagObjectsList: tagObjectsList,
  });
});

// //Get list photos object by ids
router.get("/getPhotos", async (req, res) => {
  const photosObjectsList = [];
  const photosIDS = req.query.photosIDS;
  for (var i = 0; i < photosIDS.length; i++) {
    const photoObj = await Photo.find({ _id: photosIDS[i] });
    photosObjectsList.push(photoObj);
  }
  res.send(photosObjectsList);
});

//Get tag by id
router.get("/getTag", async (req, res) => {
  const tagID = req.query.tagID;
  const tag = await Tag.find({ _id: tagID });
  res.send(tag);
});

//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
function calcCrow(lat1, lon1, lat2, lon2) {
  var R = 6371; // km
  var dLat = toRad(lat2 - lat1);
  var dLon = toRad(lon2 - lon1);
  var lat1 = toRad(lat1);
  var lat2 = toRad(lat2);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
}

// Converts numeric degrees to radians
function toRad(Value) {
  return (Value * Math.PI) / 180;
}

async function setTags(labels, photoId, userID) {
  var tags = [];
  for (const label of labels) {
    let existTag = await checkIfTagExist(label);
    if (!existTag) {
      // if tag doesn't exist create it'
      const createdTag = new Tag({
        type: label,
        photos: [photoId],
      });
      tags = [...tags, createdTag._id];
      await createdTag.save();
    } else {
      // if tag is exist add the Photo it to photos array
      existTag.photos = [...existTag.photos, photoId];
      tags = [...tags, existTag._id];
      await existTag.save();
    }
  }
  return tags;
}

module.exports = router;
