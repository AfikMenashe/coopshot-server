const multer = require('multer');
const path = require('path');

// Multer config
// module.exports = multer({
//   storage: multer.diskStorage({}),
//   // fileFilter: (req, file, cb) => {
//   // DON'T NEED THIS CODE - ALL THE UPLOADS ARE PHOTOS.... - YARIV
//   // let ext = path.extname(file.originalname);
//   // if (ext.toLowerCase() !== ".jpg" && ext.toLowerCase() !== ".jpeg" && ext.toLowerCase() !== ".png") {
//   //   cb(new Error("File type is not supported"), false);
//   //   return;
//   // }
//   //   cb(null, true);
//   // },
//   filename: (req, file, cb) => {
//     const ext = file.mimetype.split('/')[1];
//     // cb(null, `user-${req.user.id}-${Date.now()}.${ext}`);
//     cb(null, `user-123123-${Date.now()}.${ext}`);
//   },
// });

const multerStorage = multer.diskStorage({
  // To create a filename, we use the user ID and the current timestamp
  filename: (req, file, cb) => {
    const ext = file.mimetype.split('/')[1];
    // cb(null, `user-${req.user.id}-${Date.now()}.${ext}`);
    cb(null, `user-${req.user._id.toString()}-${Date.now()}.${ext}`);
  },
});
// Check if the file is actually an image
const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new AppError('Not an image! Please upload an image.', 400), false);
  }
};

const uploadOptions = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

module.exports = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});
